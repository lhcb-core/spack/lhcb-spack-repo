# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------


from spack import *


class Moore(CMakePackage):
    """Configuration and tests for the LHCb High Level Trigger application Moore."""

    homepage = "http://lhcbdoc.web.cern.ch/lhcbdoc/moore/"
    git      = "https://gitlab.cern.ch/lhcb/Moore"
    url      = "https://gitlab.cern.ch/lhcb/Moore/-/archive/v54r1/Moore-v54r1.tar.gz"

    maintainers = ['imjelde']

    version('54.1', sha256='583bd394a453cdb6d945071085d8506bb9e133e20326cea17e7c03eb36597e0e')
    version('54.0', sha256='973ef55e7c97d26f598b23b902398e122cb480eb0111b5c2722b928f28411bff')
    version('53.7', sha256='6b0dfc3e53c77aec35ea93b4a4942bcba5d0e3c773fb8312082bfd6670171492')
    version('53.6', sha256='4f1a3e65bfcc86b7bb75c92bf3d203e070722455ca1936b50a6ab4d30590fd35')
    version('53.5', sha256='3f6c82e5b6294a880ae887b8f2dd9008e098caea00252e6b2c8ed84ddda9c2fd')

    # depends_on('allen')
    # depends_on('python')

    def url_for_version(self, version):
            major = str(version[0])
            minor = str(version[1])
            url = "https://gitlab.cern.ch/lhcb/Moore/-/archive/v{0}r{1}/Moore-v{0}r{1}.tar.gz".format(major, minor)
            return url