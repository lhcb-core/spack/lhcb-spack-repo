# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------


from spack import *


class Rec(CMakePackage):
    """This project is built on top of the Gaudi, LHCb and Lbcom projects. 
    It contains components related to reconstruction. """

    homepage = "http://cern.ch/lhcbdoc/rec/"
    git      = "https://gitlab.cern.ch/lhcb/Rec"
    url      = "https://gitlab.cern.ch/lhcb/Rec/-/archive/v35r1/Rec-v35r1.tar.gz"

    maintainers = ['imjelde']

    version('35.1', sha256='1b0e6cb70f6cb41f55497e769326f1f4d7592d2016e92d34f9f9e7dc6d2191eb')
    version('35.0', sha256='b59db46a5c8b9c83a2a5b80241206d637f63eaad2943b2562f8a957b80a31c4d')
    version('34.4', sha256='c4cbcf0f71184a4c682c40d4cb73934d7446f976fea0d0e9a515de9317a545db')
    version('34.3', sha256='c34796310892d51e9db1e83fdfcf6ebf4eddfafa5e15f7b490ee6b468584b622')
    version('34.2', sha256='6f31c1b9e6efa73d9a2ddd300cf0ba4fdf5522f814bce27821ffca41fa13a2ba')
    version('34.1', sha256='99fa5ff6ee45c3de38b1851a82aff4a3b0b4b4c5f89a24c7b3106c0bd57c44ff')

    # depends_on('gaudida +optional')
    # depends_on('lbcom')

    def url_for_version(self, version):
            major = str(version[0])
            minor = str(version[1])
            url = "https://gitlab.cern.ch/lhcb/Rec/-/archive/v{0}r{1}/Rec-v{0}r{1}.tar.gz".format(major, minor)
            return url
