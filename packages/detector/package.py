# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------

from spack import *


class Detector(CMakePackage):
    """Description of the LHCb Upgrade detector using the DD4hep framework"""

    homepage = "https://gitlab.cern.ch/lhcb/Detector"
    git      = "https://gitlab.cern.ch/lhcb/Detector"
    url      = "https://gitlab.cern.ch/lhcb/Detector/-/archive/v1r2/Detector-v1r2.tar.gz"

    maintainers = ['imjelde']

    version('1.8', sha256='f132f8c0e17d89a38b79be58be3e3bb767f61b71be950ff71bda4a7da029d513')
    version('1.7', sha256='7f74ee62a1aeeebd6594237feeb388125bc68a9b854ff44194dacabb31650dac')
    version('1.6', sha256='b487dcc90c69f2edacc56b161535d82c9c624ccdb275ed2c7a607b6dbfc48fa7')
    version('1.5', sha256='a463f2bff6e52fe8fcef1142ca204947472a059a56668be416dc5b5d9392a505')
    version('1.4', sha256='1b29425e9df45db608d6f408f60bd1c783294858fec08927b030ac8e939d4a37')
    version('1.3', sha256='56b7b402767916c941da89e9673526dc5d48cdc4b4347d697cf4204e9cdc61e1')
    version('1.2', sha256='388192b573d9d8e10431730df7dabead3591432b64784d210b67ea0963039bd3')
    version('1.1', sha256='b96b25f0289fc0a8bf3f7d3c9da6a5aebc62aa51890662717f6a710fec38ca83')
    version('1.0', sha256='433c47f05190872c323bb1e3d414e9421ba785e37511fba21e2fe6fd556eceae')

    depends_on('gitconddb@0.2.0:')
    depends_on('nlohmann-json')
    depends_on('libgit2')
    depends_on('root +root7')
    depends_on('fmt@6.1.2:')
    depends_on('yaml-cpp@0.6.2:')
    depends_on('boost')
    depends_on('vc')
    depends_on('dd4hep')
    depends_on('openssl')

    def url_for_version(self, version):
        major = str(version[0])
        minor = str(version[1])
        url = "https://gitlab.cern.ch/lhcb/Detector/-/archive/v{0}r{1}/Detector-v{0}r{1}.tar.gz".format(major, minor)
        return url
