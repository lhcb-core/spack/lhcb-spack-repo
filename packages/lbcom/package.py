from spack import *


class Lbcom(CMakePackage):
    """This project is built on top of the Gaudi and LHCb projects. 
    It contains components shared by Boole and one or more other applications (Brunel, DaVinci, Moore etc.)."""

    homepage = "http://cern.ch/lhcbdoc/lbcom/"
    git      = "https://gitlab.cern.ch/lhcb/Lbcom"
    url      = "https://gitlab.cern.ch/lhcb/Lbcom/-/archive/v34r1/Lbcom-v34r1.tar.gz"

    maintainers = ['imjelde']
    
    version('34.1',  sha256='0647cb624c09524de80dd407d7f8ccd14aed63181136f46ca8d14f428a190d3f')
    version('34.0',  sha256='9c5870838d78a8ee853416851806b8193d37e90ce31e260c81d5fed5126f98bd')
    version('33.10', sha256='896299b521bbbf0fd259131637d11dfc73045a392f84ee52d4b584e33434c6e6')
    version('33.9',  sha256='2bb0a1ccef89d1a2451a465332155389a4ac891c9d2428d81fb9418914d9b5c5')
    version('33.8',  sha256='c4236cbd3bb7e38a28c13dd11c108603637761495357e0ed14d2de1086f7b59b')
    version('33.7',  sha256='cd5031762fb4b38fe3df1152bbf9f247fcf1560d90afc4f5c04fabee456c6c81')

    depends_on('lhcb')

    def url_for_version(self, version):
            major = str(version[0])
            minor = str(version[1])
            url = "https://gitlab.cern.ch/lhcb/Lbcom/-/archive/v{0}r{1}/Lbcom-v{0}r{1}.tar.gz".format(major, minor)
            return url
