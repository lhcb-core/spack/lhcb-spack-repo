# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------


from spack import *


class Lhcb(CMakePackage):
    """The LHCb project contains general purpose classes used throughout the LHCb software.
    It is built on top of the Gaudi framework."""

    homepage = "http://cern.ch/lhcbdoc/lhcb"
    git      = "https://gitlab.cern.ch/lhcb/LHCb.git"
    url      = "https://gitlab.cern.ch/lhcb/LHCb/-/archive/v54r1/LHCb-v54r1.tar.gz"

    maintainers = ['imjelde']

    version('commit1204', commit='294e780ad2c122b74999e34dd78baab30081b44d')
    version('54.4',   sha256='11b2674d10d0afc3aa9832a2aeb4d904932b0b3e896b08238277b8623e9bd24f')
    version('54.3',   sha256='eb1ac2ba89b3707e02659e9fc370eca37416e00529268da672d70665ca392083')
    version('54.2',   sha256='316bb265c6dbf76dca7d912e2072c8cf6e1a7b8f49d15d1eee0aed061d8bc658')
    version('54.1',   sha256='535de63b94b3e2cd367c5b7e80c92d3cf935c25649c54e464a1e99f4d78deb8f')
    version('54.0',   sha256='27b25d50ed45ee6a4d277da1bd52be5047c0e54f0e7e6f68afcb001e823c1f8e')
    version('53.10',  sha256='0bc14a8c0b0678e9b7d556079f5e699a567a06bc7259f1f2a02a1452bdcda633')
    version('53.9',   sha256='997c14636e604570135408807165209ede635a170b2107f8fe7053754619a9fa')
    version('53.6',   sha256='5c846c5f5f162d169718522db891b5cdd1e68171e3a69688c6afd9fb7397cb4e')

    variant('test', default=True,
        description='Include tests')

    depends_on('gaudida +optional')
    depends_on('detector')
    depends_on('aida')
    depends_on('boost@1.77: +date_time +container +iostreams +serialization +thread')
    depends_on('clhep')
    depends_on('cppgsl')
    depends_on('eigen@3')
    depends_on('fmt')
    depends_on('gsl')
    depends_on('hepmc')
    depends_on('openssl')
    depends_on('python')
    depends_on('range-v3')
    depends_on('root@6.20: +root7')
    depends_on('tbb')
    depends_on('vc@1.4.1')
    depends_on('vdt')
    depends_on('xerces-c')
    depends_on('yaml-cpp')

    depends_on('relax')

    depends_on('libgit2')
    depends_on('libzmq')
    depends_on('libsodium')

    depends_on('py-six')

    # options of full testing
    def cmake_args(self):
        args = [
            self.define_from_variant('BUILD_TESTING', 'test')
        ]
        return args
    
    def url_for_version(self, version):
        major = str(version[0])
        minor = str(version[1])
        url = "https://gitlab.cern.ch/lhcb/LHCb/-/archive/v{0}r{1}/LHCb-v{0}r{1}.tar.gz".format(major, minor)
        return url
