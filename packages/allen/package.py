# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

# ----------------------------------------------------------------------------


from spack import *


class Allen(CMakePackage):
    """Full software HLT1 reconstruction sequence on GPU."""

    homepage = "https://gitlab.cern.ch/lhcb/Allen"
    git      = "https://gitlab.cern.ch/lhcb/Allen"
    url      = "https://gitlab.cern.ch/lhcb/Allen/-/archive/v3r1/Allen-v3r1.tar.gz"

    maintainers = ['imjelde']

    version('3.1', sha256='515d916499d6cf97cffed80746e68b8682adc8ff5eb007efddd06406e4e62fb5')
    version('3.0', sha256='d826f093fa6ab615cb6a05e60d88320e44c8ed08ad38467507c0547d14033b12')
    version('2.2', sha256='e8c168ab28912607cdbf9f573b3895e10ea21df5d2b68382bc4273e970e4ae01')
    version('2.1', sha256='336101d88f034692e8076ee69d2c86e3042c8376d28dbcadc7d2f2d4b6e894da')
    version('2.0', sha256='3b531872cdf40457702e9858eeea4b7e367edd2b55102b85fe5364318e598e49')

    # depends_on('rec@34:')
    # depends_on('aida')
    # depends_on('fmt')
    # depends_on('intel-tbb')

    # depends_on('cuda')
    # depends_on('cppgsl')
    # depends_on('umesimd')
    # depends_on('pkg-config')
    # depends_on('libzmq')
    # depends_on('libsodium')

    # depends_on('python@3:')
    # depends_on('catch2')
    # depends_on('py-libclang')
    # depends_on('nlohmann-json')
    # depends_on('boost +filesystem +iostreams +thread +regex +serialization +program_options')
    # depends_on('range-v3')
    # depends_on('yaml-cpp')
    # depends_on('root')

    def url_for_version(self, version):
            major = str(version[0])
            minor = str(version[1])
            url = "https://gitlab.cern.ch/lhcb/Allen/-/archive/v{0}r{1}/Allen-v{0}r{1}.tar.gz".format(major, minor)
            return url
